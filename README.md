# radosgw_usage_exporter

This project is a copy of https://github.com/blemmenes/radosgw_usage_exporter.

We package it with [poetry](https://python-poetry.org/) and install it through
puppet pip3 provider as package. See [radosgw_usage_exporter.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/-/blob/ceph_s3multi/code/manifests/classes/radosgw_usage_exporter.pp)


## Installation notes

  * Poetry metadata section has been modified to require python38 instead of 3.10
  * The addition of `tool.poetry.scripts` in `pyproject.toml` allows for the
    creation of a wrapper placed at `/usr/local/bin` to manage the exporter as
    a service through a systemd unit.
