"""
Prometheus exporter that scrapes Ceph RADOSGW usage information.
"""
__version__ = '0.1.2'
